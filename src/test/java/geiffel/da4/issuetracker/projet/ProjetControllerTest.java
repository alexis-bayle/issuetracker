package geiffel.da4.issuetracker.projet;

import com.fasterxml.jackson.databind.ObjectMapper;
import geiffel.da4.issuetracker.commentaire.Commentaire;
import geiffel.da4.issuetracker.exceptions.ExceptionHandlingAdvice;
import geiffel.da4.issuetracker.exceptions.ResourceAlreadyExistsException;
import geiffel.da4.issuetracker.exceptions.ResourceNotFoundException;
import geiffel.da4.issuetracker.user.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = ProjetController.class)
@Import(ExceptionHandlingAdvice.class)
class ProjetControllerTest {

    @Autowired
    MockMvc mockMvc;

    private List<Projet> projets;
    private Projet mockProjet1, mockProjet2, mockProjet3, mockProjet4;

    @MockBean
    private ProjetService projetService;

    @BeforeEach
    void setUp(){
        mockProjet1 = Mockito.mock(Projet.class);

        mockProjet2 = Mockito.mock(Projet.class);
        mockProjet3 = Mockito.mock(Projet.class);
        mockProjet4 = Mockito.mock(Projet.class);
        projets = new ArrayList<>(){{
            add(mockProjet1);
            add(mockProjet2);
            add(mockProjet3);
            add(mockProjet4);
        }};

        Mockito.when(mockProjet4.getId()).thenReturn(4L);
        Mockito.when(mockProjet4.getNom()).thenReturn("lower");
    }

    @Test
    void whenQueryingRoot_shouldbeOk_andGet4Projets() throws Exception {
        Mockito.when(projetService.getAll()).thenReturn(projets);

        mockMvc.perform(get("/projets")
        ).andExpect(status().isOk()
        ).andExpect(jsonPath("$", hasSize(4))
        ).andDo(print());
    }

    @Test
    void whenGettingId4L_shouldReturnSame() throws Exception {
        Mockito.when(projetService.getById(4L)).thenReturn(projets.get(3));

        mockMvc.perform(get("/projets/4")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()
        ).andExpect(jsonPath("$.id", is(4))
        ).andExpect(jsonPath("$.nom", is("lower"))
        ).andReturn();
    }

    @Test
    void whenGettingUnexistingId_should404() throws Exception {
        Mockito.when(projetService.getById(49L)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get("/projets/49")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound()
        ).andDo(print());
    }

    @Test
    void whenCreatingNew_shouldReturnLink_andShouldBeStatusCreated() throws Exception {
        Projet new_projet = new Projet(89L, "nouveau");
        ArgumentCaptor<Projet> projet_received = ArgumentCaptor.forClass(Projet.class);
        when(projetService.create(any())).thenReturn(new_projet);

        mockMvc.perform(post("/projets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new_projet))
        ).andExpect(status().isCreated()
        ).andExpect(header().string("Location", "/projets/"+new_projet.getId())
        ).andDo(print());
    }

    @Test
    void whenCreatingWithExistingId_should404() throws Exception {
        when(projetService.create(any())).thenThrow(ResourceAlreadyExistsException.class);
        mockMvc.perform(post("/projets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(this.projets.get(2)))
        ).andExpect(status().isConflict()
        ).andDo(print());
    }

    @Test
    void whenUpdating_shouldReceiveProjetToUpdate_andReturnNoContent() throws Exception {
        Projet init_projet = projets.get(1);
        Projet updated_projet = new Projet(init_projet.getId(), "updated");
        ArgumentCaptor<Projet> projet_received = ArgumentCaptor.forClass(Projet.class);

        mockMvc.perform(put("/projets/"+init_projet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updated_projet))
        ).andExpect(status().isNoContent());
    }

    @Test
    void whenDeletingExisting_shouldCallServiceWithCorrectId_andSendNoContent() throws Exception {
        Long id = 28L;

        mockMvc.perform(delete("/projets/"+id)
        ).andExpect(status().isNoContent()
        ).andDo(print());

        ArgumentCaptor<Long> id_received = ArgumentCaptor.forClass(Long.class);
        Mockito.verify(projetService).delete(id_received.capture());
        assertEquals(id, id_received.getValue());
    }

}
