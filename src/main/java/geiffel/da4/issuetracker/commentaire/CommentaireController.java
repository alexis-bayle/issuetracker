package geiffel.da4.issuetracker.commentaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("commentaires")
@CrossOrigin(origins = "*")
public class CommentaireController {

    private CommentaireService commentaireService;

    @Autowired
    public CommentaireController(CommentaireService commentaireService) {
        this.commentaireService = commentaireService;
    }

    @GetMapping("")
    public List<Commentaire> getAll() {
        return commentaireService.getAll();
    }

    @GetMapping("{id}")
    public Commentaire getById(@PathVariable Long id) {
        return commentaireService.getById(id);
    }

    @GetMapping("issue/{id}")
    public List<Commentaire> getByIssueId(@PathVariable Long id) {
        return commentaireService.getAllByIssueCode(id);
    }

    @PostMapping("")
    public ResponseEntity<Commentaire> create(@RequestBody Commentaire commentaire) {
        Commentaire created = commentaireService.create(commentaire);
        return ResponseEntity.created(URI.create("/commentaires/"+created.getId())).build();
    }

    @PutMapping("{id}")
    public ResponseEntity<Commentaire> update(@PathVariable Long id, @RequestBody Commentaire commentaire) {
        commentaireService.update(id, commentaire);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Commentaire> delete(@PathVariable Long id) {
        commentaireService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
